FROM node:10.6-alpine

EXPOSE 8080
ENV NODE_ENV production

WORKDIR /usr/app
COPY package*.json .
RUN npm ci
COPY . .
# RUN npm run build && \
#     npm prune

CMD [ "npm", "start" ]

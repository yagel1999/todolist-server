declare namespace NodeJS {
	export interface ProcessEnv {
		NODE_ENV: 'development' | 'production';
		DB_NAME: string;
		DB_USER: string;
		DB_PASS: string;
		DB_HOST: string;
		DB_READ_PORT: string;
		DB_WRITE_PORT: string;
	}
}

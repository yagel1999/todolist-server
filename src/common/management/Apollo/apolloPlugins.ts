import { Container } from 'typedi';
import { Disposable } from 'graphql-ws';
import type { ApolloServerPlugin } from 'apollo-server-plugin-base';
import { GraphQLRequestContextWillSendResponse } from 'apollo-server-types';
import { IContext } from '../../models';

interface DrainWSServerOptions {
	wsServerCleanup: Disposable;
}

export { ApolloServerPluginDrainHttpServer as drainHttpServer } from 'apollo-server-core';

export const drainWSServer = ({
	wsServerCleanup
}: DrainWSServerOptions): ApolloServerPlugin => ({
	async serverWillStart() {
		return {
			async drainServer() {
				wsServerCleanup.dispose();
			}
		};
	}
});

export const drainContainerInstance = (): ApolloServerPlugin => ({
	async requestDidStart() {
		return {
			async willSendResponse({
				context: { containerId }
			}: GraphQLRequestContextWillSendResponse<IContext>) {
				Container.reset(containerId);

				/*
				// Reset all open containers
				// Note that Container.instances is a PRIVATE data member 
				((Container as any).instances as ContainerInstance[]).forEach(
					(instance: ContainerInstance) => {
						console.log(
							`${instance.id} container instance was left hanging in memory, we're cleaning it up`
						);
						Container.reset(instance.id);
					}
				);
				*/
			}
		};
	}
});

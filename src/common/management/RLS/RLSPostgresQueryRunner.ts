/* eslint-disable @typescript-eslint/no-explicit-any */
import { ReplicationMode } from 'typeorm';
import { PostgresDriver } from 'typeorm/driver/postgres/PostgresDriver';
import { PostgresQueryRunner } from 'typeorm/driver/postgres/PostgresQueryRunner';
import { IsolationLevel } from 'typeorm/driver/types/IsolationLevel';
import { TenancyModelOptions } from '../../models';

export class RLSPostgresQueryRunner extends PostgresQueryRunner {
	private tenantId: number;
	private isTransactionCommand = false;

	constructor(
		driver: PostgresDriver,
		mode: ReplicationMode,
		tenancyModelOptions: TenancyModelOptions
	) {
		super(driver, mode);
		this.tenantId = tenancyModelOptions.tenantId;
	}

	async query(
		queryString: string,
		params?: any[],
		useStructuredResult?: boolean
	): Promise<any> {
		if (!this.isTransactionCommand) {
			await super.query(`set session role "tenant-rls";`);
			await super.query(`set "rls.tenant_id" = '${this.tenantId}';`);
		}

		const result = await super.query(queryString, params, useStructuredResult);

		if (!this.isTransactionCommand) {
			await super.query(`reset "rls.tenant_id";`);
			await super.query(`reset role;`);
		}

		return result;
	}

	async startTransaction(isolationLevel?: IsolationLevel): Promise<void> {
		this.isTransactionCommand = true;
		await super.startTransaction(isolationLevel);
		this.isTransactionCommand = false;
	}

	async commitTransaction(): Promise<void> {
		this.isTransactionCommand = true;
		await super.commitTransaction();
		this.isTransactionCommand = false;
	}

	async rollbackTransaction(): Promise<void> {
		this.isTransactionCommand = true;
		await super.rollbackTransaction();
		this.isTransactionCommand = false;
	}
}

import { ConnectionOptions } from 'typeorm';
import { PostgresConnectionCredentialsOptions } from 'typeorm/driver/postgres/PostgresConnectionCredentialsOptions';
import { isDev } from '../common/consts';

const baseDBCredentials: PostgresConnectionCredentialsOptions = {
	host: process.env.DB_HOST,
	database: process.env.DB_NAME,
	username: process.env.DB_USER,
	password: process.env.DB_PASS,
};

const ormConfig: ConnectionOptions = {
	type: 'postgres',
	schema: 'public',
	synchronize: isDev,
	logging: isDev,
	migrationsRun: !isDev,
	entities: ['src/schemas/**/entity.ts'],
	migrations: ['src/migrations/*.ts'],
	cli: {
		entitiesDir: 'src/schemas',
		migrationsDir: 'src/migrations'
	},
	replication: {
		master: {
			...baseDBCredentials,
			port: parseInt(process.env.DB_WRITE_PORT)
		},
		slaves: [{
			...baseDBCredentials,
			port: parseInt(process.env.DB_READ_PORT)
		}]
	}
};

export default ormConfig;

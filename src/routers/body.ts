import { Router, json } from 'express';
import cors from 'cors';

const bodyRouter: Router = Router();

bodyRouter.use(cors());
bodyRouter.use(json());

export default bodyRouter;

import {
	Resolver,
	Mutation,
	Subscription,
	PubSub,
	Publisher,
	Arg,
	Int,
	Query,
	FieldResolver,
	Root,
	Authorized
} from 'type-graphql';
import { Logger } from 'winston';
import { Repository } from 'typeorm';
import { Inject, Service } from 'typedi';
import BaseResolver from '../BaseResolver';
import { SubscriptionActions } from '../../common/models';
import { RLSConnection } from '../../common/management/RLS';
import { CONNECTION_DI_KEY, LOGGER_DI_KEY } from '../../common/consts';
import { Todo, UpdateTodoInput, CreateTodoInput, User } from '../entities';
import { TodoSubscription, TodoSubscriptionPayload } from './subscription';

const TOPIC: string = 'TODOS';

@Service()
@Resolver(Todo)
export default class TodoResolver extends BaseResolver {
	private todosRepository: Repository<Todo>;

	constructor(
		@Inject(LOGGER_DI_KEY) logger: Logger,
		@Inject(CONNECTION_DI_KEY) connection: RLSConnection
	) {
		super(Todo, logger);
		this.todosRepository = connection.getRepository(Todo);
	}

	@Authorized('WRITE')
	@Mutation(() => Todo)
	async createTodo(
		@Arg('createInput', () => CreateTodoInput) createInput: CreateTodoInput,
		@PubSub(TOPIC) publish: Publisher<TodoSubscriptionPayload>
	): Promise<Todo> {
		this.logger.info(`Creating new Todo`, { options: createInput });

		const todo: Todo = await this.todosRepository.save(
			this.todosRepository.create(createInput)
		);

		publish({
			todoId: todo.id,
			action: SubscriptionActions.CREATE
		});

		return todo;
	}

	@Authorized('WRITE')
	@Mutation(() => Todo)
	async updateTodo(
		@Arg('updateInput', () => UpdateTodoInput) updateInput: UpdateTodoInput,
		@PubSub(TOPIC) publish: Publisher<TodoSubscriptionPayload>
	): Promise<Todo> {
		this.logger.info(`Updating Todo`, { options: updateInput });

		const updatedTodo: Todo = await this.todosRepository.save(updateInput);

		publish({
			todoId: updatedTodo.id,
			action: SubscriptionActions.UPDATE
		});

		return updatedTodo;
	}

	@Authorized('WRITE')
	@Mutation(() => Boolean)
	async deleteTodo(
		@Arg('id') id: string,
		@PubSub(TOPIC) publish: Publisher<TodoSubscriptionPayload>
	): Promise<boolean> {
		this.logger.info(`Deleting Todo by Id`, {
			options: {
				id
			}
		});

		const didDelete: boolean =
			(await this.todosRepository.delete({ id })).affected === 1;

		publish({
			todoId: id,
			action: SubscriptionActions.DELETE
		});

		return didDelete;
	}

	@Authorized()
	@Query(() => [Todo])
	async todos(): Promise<Todo[]> {
		this.logger.info(`Fetching all Todos`);

		return await this.todosRepository.find();
	}

	@Authorized()
	@Query(() => Todo)
	async todoById(
		@Arg('todoId', () => Int) todoId: string
	): Promise<Todo | undefined> {
		this.logger.info(`Fetching todo by id: ${todoId}`);

		return await this.todosRepository.findOne(todoId);
	}

	@Authorized()
	@Subscription(() => TodoSubscription, {
		topics: TOPIC
	})
	async todoChanges(@Root() payload: TodoSubscriptionPayload) {
		return new TodoSubscription(
			payload.action,
			payload.action !== SubscriptionActions.DELETE
				? await this.todosRepository.findOne(payload.todoId)
				: undefined
		);
	}

	@Authorized()
	@Query(() => [Todo])
	async todosByUserId(
		@Arg('userId', () => Int) userId: string
	): Promise<Todo[]> {
		this.logger.info(`Fetching Todos by User Id`, {
			options: {
				userId
			}
		});

		return await this.todosRepository
			.createQueryBuilder('todo')
			.innerJoin(User, 'user', '"todo"."owningUserId" = "user"."id"')
			.where('"todo"."owningUserId" = :userId', { userId })
			.getMany();
	}

	@FieldResolver(() => User)
	async user(@Root() parent: Todo): Promise<User> {
		return await User.findOneOrFail(parent.owningUserId);
	}
}

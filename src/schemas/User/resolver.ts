import {
	Resolver,
	Query,
	FieldResolver,
	Root,
	Mutation,
	Arg
} from 'type-graphql';
import { Service } from 'typedi';
import BaseResolver from '../BaseResolver';
import User, { CreateUserInput, UpdateUserInput } from './entity';

@Service()
@Resolver(User)
export default class UserResolver extends BaseResolver {
	constructor() {
		super(User);
	}

	@Query(() => [User])
	async users(): Promise<User[]> {
		this.logger.info('Fetching all Users');

		return await User.find();
	}

	@Mutation(() => User)
	async createUser(
		@Arg('createInput', () => CreateUserInput) createInput: CreateUserInput
	): Promise<User> {
		this.logger.info(`Creating a new User`, { options: createInput });

		return await User.create(createInput).save();
	}

	@Mutation(() => User)
	async updateUser(
		@Arg('updateInput', () => UpdateUserInput) updateInput: UpdateUserInput
	): Promise<User> {
		this.logger.info(`Updating a User`, { options: updateInput });

		return await User.create(updateInput).save();
	}

	@FieldResolver()
	name(@Root() parent: User): string {
		return `${parent.firstName} ${parent.lastName}`;
	}
}

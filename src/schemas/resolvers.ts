export { default as TodosResolver } from './Todo/resolver';
export { default as UsersResolver } from './User/resolver';
export { default as TenantResolver } from './Tenant/resolver';
